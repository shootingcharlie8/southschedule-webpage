<?php
$down = false;
if(isset($_GET['dev']))
{
	echo "dev mode enabled";
}
else if($down)
{
	header("Location: /down.html"); /* Redirect browser */
	exit();
}
?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" type="text/css" href="../css/normalize.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-slider.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-switch.min.css">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,900,500|Raleway:400,100' rel='stylesheet' type='text/css'>
	<!-- <link rel="stylesheet" type="text/css" href="../css/styles.css"> -->
	<link rel="stylesheet" type="text/css" href="../css/schedule.css">
	<link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/oljpfipfiljocopmadffodjgkgaiflnl">
	<script src="../js/resources/jquery-2.1.4.min.js"></script>
	<script src="../js/resources/bootstrap.min.js"></script>
	<script src="../js/resources/bootstrap-slider.min.js"></script>
	<script src="../js/resources/bootstrap-switch.min.js"></script>
	<script src="../js/resources/flowtype.js"></script>

	<title>South Schedule</title>
	<meta name="title" content="South Schedule">
	<meta name="description" content="Realtime bell schedule and class period countdown for Maine South High School">
	<meta name="author" content="Philip Melidosian">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="maine, south, mainedb, district, 207, dashboard, Maine Dashboard, time, period, southschedule, South Schedule">

	<link rel="icon" href="img/icon.png">
	<link rel="apple-touch-icon" sizes="120x120" href="Resources/icons/iconLarge.png">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-61705441-2"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-61705441-2');
	</script>
</head>
<body>
	<div id="nav-container" class='container-fluid' style="  z-index: 100">
		<div id="brand-container" class="col-xs-12" style="font-size: 40px;position:  relative;">
			<a href="../"><button type="button" style="/* float:left; *//* vertical-align: middle; */position: absolute;left: .5em;top: 50%;transform: translate(0,-50%);" class="btn btn-primary" id="back">Go Back</button></a>
			<p style="text-align:center;"><span style="font-weight:100">SOUTH</span> <span style="font-weight: 400;">SCHEDULE</span></p>
		</div>
	</div>
	<div style="background: #4096EE; padding-bottom: 10px; padding-top: 10px; font-size: 20px;" class="container-fluid" id="clock-container">
		<div class="col-md-12 schedule-container" id="schedule-container">
			<h3 class="text-center container-title">Week Day Schedules</h3>
			<div class="table-responsive" id="FutureScheduleContainer">
				<div id="FutureSchedule" class="table">
					<div class="text-center">
						<button type="button" class="btn btn-primary" onclick="showSchedule(2)" id="day2">Monday</button>
						<button type="button" class="btn btn-primary" onclick="showSchedule(3)" id="day3">Tuesday</button>
						<button type="button" class="btn btn-primary" onclick="showSchedule(4)" id="day4">Wednsday</button>
						<button type="button" class="btn btn-primary" onclick="showSchedule(5)" id="day5">Thursday</button>
						<button type="button" class="btn btn-primary" onclick="showSchedule(6)" id="day6">Friday</button>
					</div>
					<p> <i>Note: These schedules are for the regular week days, and do not reflect any "special" days.</i></p>
				</div>
		</div>


		</div>
	</div>
	<div class="container-fluid" id="footer-container">
		<br>
		<p class="text-center">Programmed and maintained by Philip (Charlie) Melidosian - Maine South High School class of 2019.</p>
		<p class="text-center"> &copy; 2019 All Rights Reserved. </p>
		<br>
	</div>
</body>
<footer>
	<script src="schedulepage.js?noCache=<?php echo md5_file("schedulepage.js"); ?>"></script>
	<script>
		$(document).ready(
			function()
			{
				window.addEventListener("hashchange", function() { scrollBy(0, -50) });
				$("#eventsContainer").hide();
				$("#editContainer").hide();
				$("#editToggle").click( function() {
					$("#editToggle").slideUp();
					setTimeout( function() { $("#editContainer").slideDown(); }, 500);
				});
			}
		);


		$('#clock-container').addClass('animated slideInDown');
	</script>
	<script type="text/javascript">
		update();
		function update()
		{
			// var parentHeight = $('#clock-container').height();
			// var childHeight = $('#clock-content-container').height();
			// //$('#clock-content-container').css('margin-top', (parentHeight - childHeight) / 2);
			//
			// var navWidth = $('#nav-container').width();
			// var brandWidth = $('#brand-container').width();
			// setTimeout( function() { update();}, 10);
		}
	</script>
</footer>
</html>
