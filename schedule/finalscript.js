
function showSchedule(scheduleNum)
{
	for (let i=2; i<7; i++) {
			var table = document.getElementById("sched-day-"+i);
			table.style.display = "none";
			var button = document.getElementById("day"+i);
			button.classList.remove("active");

	}
	var button = document.getElementById("day"+scheduleNum);
	if(scheduleNum==1 || scheduleNum ==7)
	{
		table = document.getElementById("sched-day-"+2);
		table.style.display = "table";
		var button = document.getElementById("day"+2);
		button.classList.add("active");

	}
	else {
		table = document.getElementById("sched-day-"+scheduleNum);
		table.style.display = "table";
		button.classList.add("active");

	}

}
window.onload = function () {
	var d = new Date();
	var n = d.getDay()
	console.log(d);
	showSchedule(n+1);
};
