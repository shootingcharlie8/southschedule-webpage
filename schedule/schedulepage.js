var schedule = [];
var curTime = 0;
var curPeriod = "";
var heartBeat = true;
window.onload = function () {
	WebSocketTest();
};
function converRawTo12(time)
{
		var minutes = time.substr(time.length -2);
		var hour = time.substr(0, time.length -2);
		if(hour>12)
		{
				hour=hour-12;
		}
		return hour+":"+minutes
}
function getTimeDif(tm1, tm2)
{
		var offset1 = parseInt((tm1.substr(0, tm1.length -2)*60))+parseInt((tm1.substr(tm1.length -2)))
		var offset2 = parseInt((tm2.substr(0, tm2.length -2)*60))+parseInt((tm2.substr(tm2.length -2)))
		return offset2-offset1;
}
function toTitleCase(str) {
	// Credits: https://stackoverflow.com/a/4878800
	return str.replace(/\w\S*/g, function(txt){
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}

function colorTblRow()
{
}
function addSchedule(type, schedule)
{
	if(type == 0)
	{
		var table = document.getElementById("currSchedule");
		if(table != null)
		{
			var k=0;
			console.log(table);
			while(table.hasChildNodes())
			{
				table.removeChild(table.firstChild);
			}
			for (let i=0; i<schedule.length; i+=3) {
				var row = table.insertRow(k);
				var cell1 = row.insertCell(0);
				var cell2 = row.insertCell(1);
				var cell3 = row.insertCell(2);
				cell1.innerHTML = schedule[i];
				cell2.innerHTML = converRawTo12(schedule[i+1]);
				cell3.innerHTML = converRawTo12(schedule[i+2]);
				k++;
			}
		}
	}
	else if (type>1 && type <7)
	{
		var table = document.getElementById("FutureSchedule");
		var k=0;
		table.innerHTML += "<table id='sched-day-"+type+"' class='table'></table>";
		table = document.getElementById("sched-day-"+type);
		while(table.hasChildNodes())  //fixed issue with repeating schedule on Tuesday and Wednsday
		{
				table.removeChild(table.firstChild);
		}
		schedule = schedule.slice(schedule.indexOf(",[")+2,schedule.length-1);
		schedule = schedule.split(",");
		for (let i=0; i<schedule.length; i+=3) {
			var row = table.insertRow(k);
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);
			var cell3 = row.insertCell(2);
			cell1.innerHTML = schedule[i];
			cell2.innerHTML = converRawTo12(schedule[i+1]);
			cell3.innerHTML = converRawTo12(schedule[i+2]);
			k++;
		}
	}
}

function addDate()
{
		var d = new Date();
		var date = d.getMonth()+1	+ "/" + d.getDate() + "/" + d.getFullYear();
		const currDate = document.getElementById("currDate");
		currDate.innerHTML=date;
}
function parseMessage(data)
{
	if (data.includes("SCHEDULE")) {
			schedule = data.replace("SCHEDULE=[", "").replace("]","").split(",");
			addSchedule(0,schedule);
	}
	if (data.includes("AllSchedules:{")) {
			// schedule = data
			// console.log(schedule);
			schedule = data.replace("AllSchedules:{","").split("},{");
			// schedule = schedule.split(",");
			// console.log(schedule.length);
			for(i=0;i<schedule.length; i++)
			{
				var array = [];
				array = schedule[i].split(",",2);
				// console.log(schedule[i]);
				// console.log(schedule[i].indexOf('['));
				// console.log(schedule[i].slice(schedule[i].indexOf(",[")+2));
				addSchedule(array[1],schedule[i]);
			}
			var d = new Date();
			var n = d.getDay()
			showSchedule(n+1);
	}
	else if (data.includes("PONG")) {
			// console.log("Got Pong!");
			heartBeat = true;
	}
}
function showSchedule(scheduleNum)
{
	for (let i=2; i<7; i++) {
			var table = document.getElementById("sched-day-"+i);
			table.style.display = "none";
			var button = document.getElementById("day"+i);
			button.classList.remove("active");

	}
	var button = document.getElementById("day"+scheduleNum);
	if(scheduleNum==1 || scheduleNum ==7)
	{
		table = document.getElementById("sched-day-"+2);
		table.style.display = "table";
		var button = document.getElementById("day"+2);
		button.classList.add("active");

	}
	else {
		table = document.getElementById("sched-day-"+scheduleNum);
		table.style.display = "table";
		button.classList.add("active");

	}

}
function checkConn(ws)
{
	setTimeout(function () {

		ws.send("PING")
		// console.log("Sending pong!");
		var i=0;
		while( i<5)
		{
			setTimeout(function () {
				if(heartBeat==true)
				{
					// console.log("Ending loop");
					i=5;
				}
			}, 1000);
			i++;
		}
		// console.log(heartBeat);

		if (heartBeat == false && i>=5)
		{
			// console.log("No heartBeat, reloading!");
			// location.reload();
		}
		heartBeat=false;
		checkConn(ws);
}, 5000);}

function WebSocketTest()
{
	 if ("WebSocket" in window)
	 {
		 console.log(window.location.hostname);

		  if(window.location.hostname == "dev.southschedule.net")
		  {
		 	 var ws = new WebSocket("ws://backend.southschedule.net:9090");
		  }
		  else if (window.location.hostname == "local.southschedule.net") {
		 	 var ws = new WebSocket("ws://local.southschedule.net:9090");
		  }
		  else {
		 	 var ws = new WebSocket("ws://backend.southschedule.net:8080");
		  }
 			ws.onopen = function()
			{
				checkConn(ws);
				ws.send("allschedules");

			};
			ws.onmessage = function (evt)
			{
				 var received_msg = evt.data;
				 parseMessage(received_msg);
			};
			ws.onclose = function()
			{
				setTimeout(function () {
						// location.reload();
				}, 5000);
				// console.log("Connection is closed...");
			};
			function onError(evt)
			{
				// console.log("ERROR!");
			}
	 }
	 else
	 {
			// The browser doesn't support WebSocket
			alert("WebSocket NOT supported by your Browser!");
	 }
}
function toTitleCase(str) {
	// Credits: https://stackoverflow.com/a/4878800
	return str.replace(/\w\S*/g, function(txt){
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}
