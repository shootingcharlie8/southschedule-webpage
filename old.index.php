<?php
$down = false;
if(isset($_GET['dev']))
{
	echo "dev mode enabled";
}
else if($down)
{
	header("Location: /down.html"); /* Redirect browser */
	exit();
}
?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-slider.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-switch.min.css">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,900,500|Raleway:400,100' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/oljpfipfiljocopmadffodjgkgaiflnl">



	<script src="js/resources/jquery-2.1.4.min.js"></script>
	<script src="js/resources/bootstrap.min.js"></script>
	<script src="js/resources/bootstrap-slider.min.js"></script>
	<script src="js/resources/bootstrap-switch.min.js"></script>
	<script src="js/resources/flowtype.js"></script>
	<title>South Schedule</title>
	<meta name="title" content="MaineDB">
	<meta name="description" content="Realtime bell schedule and class period countdown for Maine South High School">
	<meta name="author" content="Philip Melidosian">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="maine, south, mainedb, district, 207, dashboard, Maine Dashboard, time, period, southschedule, South Schedule">

	<link rel="icon" href="img/icon.png">
	<link rel="apple-touch-icon" sizes="120x120" href="Resources/icons/iconLarge.png">

	<script>
	$(document).ready( function() {
		$(document).on('click', '#main-nav li:nth-child(2)', function () {
			window.scrollBy(0, 100);
		});


	});


</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-61705441-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-61705441-2');
</script>
</head>
<body>

	<div id="nav-container" class='container-fluid' style="  z-index: 100">
		<div id="brand-container" class='col-xs-12' style="font-size: 40px; display: inline-block;">
			<p class="text-center" ><span style="font-weight:100">SOUTH</span> <span style="font-weight: 400;">SCHEDULE</span></p>
		</div>
	</div>
	<script>
	$('div').flowtype({
		minFont : 30
	});
</script>



<div style="background: #4096EE; padding-bottom: 10px; padding-top: 10px; font-size: 20px;" class="container-fluid" id="clock-container">
	<br>
	<div class="col-md-6" id="clock-content-container">

		<p class="text-center"><span id="currPeriod">loading...</span></p>
		<div id="circle" class="hide-on-project">
			<p class="text-center" id="currTime">...</p>
			<p class="text-center" id="currDate">&nbsp;</p>
		</div>

		<p class="text-center" id="currCountdown">&nbsp;</p>
		<p class="text-center" id="currDaytype">&nbsp;</p>



		<p class="text-center hidden-xs hide-on-project"><i id="randomquote"></i></p>
		<p class="text-center hidden-xs hide-on-project">Want South Schedule at the tip of your finger? Add the chrome extension to access South Schedule without leaving your tab.<br />
			<button type="button" class="btn btn-primary text-center hidden-xs" onclick="chrome.webstore.install()" id="install-button">Add to Chrome</button>
		</p>
		<p class="text-center hide-on-project">If you spot a problem, send an email or Hangout message me at <a style="color: #E8E3D4;" href="mailto:cmelidosian@s207.org">cmelidosian@s207.org</a>. <br /></p>
		<!-- onclick="chrome.webstore.install()" id="install-button"-->
		<script>
		if (chrome.app.isInstalled) {
			document.getElementById('install-button').style.display = 'none';
		}
	</script>
	<!-- <p class="text-center hidden-xs"><em style="white-space:pre-wrap;">Did you know? </em> You can use South Schedule next year to keep track of the confusing block schedule!</p> -->
</div>

<div class="col-md-6" id="schedule-container">
	<div id="title-container" class="col-xs-12" style="font-size: 40px;position:  relative;">
		<h3 class="text-center container-title">Schedule</h3>
		<a href="schedule/"><button type="button" style="position: absolute;left: .5em;top: 50%;transform: translate(0,-50%);" class="btn btn-primary" id="back">Daily Schedules</button></a>
		<!--<a href="schedule/finals.html"><button type="button" style="position: absolute;right: .5em;top: 50%;transform: translate(0,-50%);" class="btn btn-primary" id="back">Final Schedule</button></a>-->
	</div>
	<!--			<h5 class="text-center" id="scheduleLoadingMessage">loading...</h5>-->
	<div class="table-responsive" style="width: 100%;" id="currScheduleContainer">
		<table id="currSchedule" class="table">
		</table>
	</div>
</div>
</div>
<!-- <br> -->

</body>
<footer>
	<div class="container-fluid" id="footer-container">
		<!-- <button type="button" style="position: relative;left: .5em;/* top: 50%; */transform: translate(0,100%);" class="btn btn-primary" id="projector" onclick="project()">Projector Mode</button> -->
		<br>
		<p class="text-center">Programmed and maintained by Philip (Charlie) Melidosian - Maine South High School class of 2019.</p>
		<p class="text-center"> &copy; 2019 All Rights Reserved. </p>

		<br>
	</div>
	<script src="js/main.js?noCache=<?php echo md5_file("js/main.js"); ?>"></script>
	<!-- <script src="js/projector.js?noCache=<?php //echo md5_file("js/projector.js"); ?>"></script> -->
	<script>
	$(document).ready(
		function()
		{
			window.addEventListener("hashchange", function() { scrollBy(0, -50) });
			$("#eventsContainer").hide();
			$("#editContainer").hide();
			$("#editToggle").click( function() {
				$("#editToggle").slideUp();
				setTimeout( function() { $("#editContainer").slideDown(); }, 500);
			});
		}
	);


	$('#clock-container').addClass('animated slideInDown');
</script>
<script type="text/javascript">
update();
function update()
{
	var parentHeight = $('#clock-container').height();
	var childHeight = $('#clock-content-container').height();
	//$('#clock-content-container').css('margin-top', (parentHeight - childHeight) / 2);

	var navWidth = $('#nav-container').width();
	var brandWidth = $('#brand-container').width();
	setTimeout( function() { update();}, 10);
}


</script>
</footer>
</html>
