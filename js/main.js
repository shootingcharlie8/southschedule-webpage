var schedule = [];
var curTime = 0;
var curPeriod = "";
var heartBeat = true;
window.onload = function () {
	WebSocketTest();
	randomquote();
};
function converRawTo12(time)
{
		var minutes = time.substr(time.length -2);
		var hour = time.substr(0, time.length -2);
		if(hour>12)
		{
				hour=hour-12;
		}
		return hour+":"+minutes
}
function getTimeDif(tm1, tm2)
{
		var offset1 = parseInt((tm1.substr(0, tm1.length -2)*60))+parseInt((tm1.substr(tm1.length -2)))
		var offset2 = parseInt((tm2.substr(0, tm2.length -2)*60))+parseInt((tm2.substr(tm2.length -2)))
		return offset2-offset1;
}
function toTitleCase(str) {
	// Credits: https://stackoverflow.com/a/4878800
	return str.replace(/\w\S*/g, function(txt){
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}
function addPeriod(period, schedule, pEnd, nextPeriod, nextEnd)
{
		const d = document.getElementById("currPeriod");
		d.innerHTML=period;
		const b = document.getElementById("currCountdown");
		var output=period
		if(getTimeDif(curTime,pEnd)>=0) {
			document.title = "("+getTimeDif(curTime,pEnd)+" mins) South Schedule";
		} else {
			document.title = "South Schedule";
		}	
		if(pEnd == 1520)
		{
			output="School"
			b.innerHTML=output+" ends at "+converRawTo12(pEnd)+" ("+getTimeDif(curTime,pEnd) + " minutes)";
		}
		else if (period=="No School") {
			b.innerHTML="No School ATM";
		}
		else
		{
			b.innerHTML=output+" ends at "+converRawTo12(pEnd)+" ("+getTimeDif(curTime,pEnd) + " minutes)</br>"+nextPeriod+" ends at "+converRawTo12(nextEnd)+" ("+getTimeDif(curTime,nextEnd) + " minutes)";
		}
}
function colorTblRow()
{
}
function addSchedule(schedule)
{
		var table = document.getElementById("currSchedule");
		var k=0;
		while(table.hasChildNodes())
		{
			 table.removeChild(table.firstChild);
		}
		for (let i=0; i<schedule.length; i+=3) {
				var row = table.insertRow(k);
				// Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
				var cell1 = row.insertCell(0);
				var cell2 = row.insertCell(1);
				var cell3 = row.insertCell(2);
				// Add some text to the new cells:
				cell1.innerHTML = schedule[i];
				cell2.innerHTML = converRawTo12(schedule[i+1]);
				cell3.innerHTML = converRawTo12(schedule[i+2]);
				//        cell2.innerHTML = "NEW CELL2";
				k++;
		}
}
function changeClock(period)
{
		const d = document.getElementById("currTime");
		d.innerHTML=period;

}
function addDate()
{
		var d = new Date();
		var date = d.getMonth()+1	+ "/" + d.getDate() + "/" + d.getFullYear();
		const currDate = document.getElementById("currDate");
		currDate.innerHTML=date;

}
function parseMessage(data)
{
		if (data.includes("SCHEDULE")) {
				schedule = data.replace("SCHEDULE=[", "").replace("]","").split(",");
				addSchedule(schedule);
		}
		else if (data.includes("PONG")) {
				// console.log("Got Pong!");
				heartBeat = true;
		}
		else
		{
				addDate();
				var timedata = data.split("|");
				curTime= timedata[0];
				changeClock(converRawTo12(timedata[0]));
				const dayTypeElm = document.getElementById("currDaytype");
				dayTypeElm.innerHTML=timedata[2];
				addPeriod(timedata[1], schedule, timedata[3], timedata[4], timedata[5]);
		}
}
function checkConn(ws)
{
	// var heartBeat = false;
	// console.log(ws.readyState);
	setTimeout(function () {

		ws.send("PING")
		// console.log("Sending pong!");
		var i=0;
		while( i<5)
		{
			setTimeout(function () {
				// Do Something Here
				// Then recall the parent function to
				// create a recursive loop.
				if(heartBeat==true)
				{
					// console.log("Ending loop");
					i=5;
				}
			}, 1000);
			i++;
		}
		// console.log(heartBeat);

		if (heartBeat == false && i>=5)
		{
			console.log("No heartBeat, reloading!");
			location.reload();
		}
		heartBeat=false;
		checkConn(ws);
}, 5000);}

function WebSocketTest()
{
	 if ("WebSocket" in window)
	 {
		console.log(window.location.hostname);

		if(window.location.hostname == "dev.southschedule.net")
		{
			var ws = new WebSocket("ws://backend.southschedule.net:9090");
		}
		else if (window.location.hostname == "local.southschedule.net") {
			var ws = new WebSocket("ws://local.southschedule.net:9090");
		}
		else {
			var ws = new WebSocket("ws://backend.southschedule.net:8080");
		}
			ws.onopen = function()
			{
				checkConn(ws);
			};
			ws.onmessage = function (evt)
			{
				 var received_msg = evt.data;
				 parseMessage(received_msg);
			};
			ws.onclose = function()
			{
				setTimeout(function () {
						location.reload();
				}, 5000);
				console.log("Connection is closed...");
			};
			function onError(evt)
			{
				console.log("ERROR!");
			}
	 }
	 else
	 {
			// The browser doesn't support WebSocket
			alert("WebSocket NOT supported by your Browser!");
	 }
}
function randomquote()
{
	// Script Credit: http://www.javascriptkit.com/script/cut15.shtml
	// Quote Credit: https://www.codeofliving.com/quotes/37-motivational-school-quotes-your-educational-journey
	var ar = new Array(31)
	ar[0] = "Dont stress. Do your best. Forget the rest. - Anonymous"
	ar[1] = "Education is our passport to the future. For tomorrow belongs to the people who prepare it today. - Malcolm X"
	ar[2] = "When educating the minds of our youth, we must not forget to educate their hearts. - Dalai Lama"
	ar[3] = "Education is what remains after one has forgotten what one has learned in school. - Albert Einstein"
	ar[4] = "In school, youre taught a lesson and then given a test. In life, youre given a test that teaches you a lesson. - Tom Bodett"
	ar[5] = "True terror is to wake up one morning and discover that your high school class is running the country. - Kurt Vonnegut"
	ar[6] = "All children start their school careers with sparkling imaginations, fertile minds, and a willingness to take risks with what they think. - Ken Robinson"
	ar[7] = "We dont stop going to school when we graduate. - Carol Burnett"
	ar[8] = "A journey of a thousand miles begins with a single step. - Confucius"
	ar[9] = "What makes a child gifted and talented may not always be good grades in school, but a different way of looking at the world and learning. - Chuck Grassley"
	ar[10] = "I was a smart kid, but I hated school. - Eminem"
	ar[11] = "Children should learn that reading is pleasure, not just something that teachers make you do in school. - Beverly Cleary"
	ar[12] = "School is a building which has four walls with tomorrow inside. - Lon Watters"
	ar[13] = "Im not telling you its going to be easy- Im telling you its going to be worth it. - Art Williams"
	ar[14] = "Nothing is impossible the word itself says Im possible. - Audrey Hepburn"
	ar[15] = "Intelligence plus character that is the goal of true education. - Martin Luther King Jr."
	ar[16] = "You dont have to be great to get started, but you have to get started to be great. - Les Brown"
	ar[17] = "Ive got a theory that if you give 100 percent all the time, somehow things will work out in the end. - Larry Bird"
	ar[18] = "Everybody is a genius. But if you judge a fish by its ability to climb a tree, it will live its whole life believing that it is stupid. - Albert Einstein"
	ar[19] = "Do the best you can until you know better. Then when you know better, do better. - Maya Angelou"
	ar[20] = "You may be disappointed if you fail, but youll be doomed if you dont try. - Beverly Hills"
	ar[21] = "Whether you think you can or think you cant youre right. - Henry Ford"
	ar[22] = "Success is the sum of small efforts, repeated day in and day out. - Robert Collier"
	ar[23] = "Inspiration exists, but it has to find you working. - Pablo Picasso"
	ar[24] = "I never dreamt of success. I worked for it. - Estee Lauder"
	ar[25] = "This is a new year. A new beginning. And things will change. - Taylor Swift"
	ar[26] = "Im not going to school just for the academics. I wanted to share ideas, to be around people who are passionate about learning. - Emma Watson"
	ar[27] = "You learn something every day if you pay attention. - Ray LeBlond"
	ar[28] = "Learning is a treasure that will follow its owner everywhere. - Chinese Proverb"
	ar[29] = "Success is stumbling from failure to failure with no loss of enthusiasm. - Winston Churchill"
	ar[30] = "Every year, many, many stupid people graduate from college. And if they can do it, so can you. - John Green"
	var now = new Date();
	var sec = now.getSeconds();
	document.getElementById("randomquote").innerHTML="Random Quote: " + ar[sec % 20];
}
function toTitleCase(str) {
	// Credits: https://stackoverflow.com/a/4878800
	return str.replace(/\w\S*/g, function(txt){
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}
